import groovy.json.JsonOutput

pipeline {
    agent {
        node {
            label 'maven'
        }
    }
    environment {
      test_tag = 'latest'
      test_project = 'app-test'
      qa_project = 'app-qa'
      prod_project = 'app-prod'
      application = 'wildfly-demo'
      git_url = 'git@bitbucket.org:rahnarsson/cicd-demo.git'
      branch_to_build = 'master'
      sonar_project = 'wildfly-demo'
      // SKIP_TLS = true
    }
    options {
        timeout(time: 20, unit: 'MINUTES')
    }
    stages {
        stage('Info') {
            steps {
                echo "Project: ${test_project}, \nApplication: ${application}"
            }
        }

         stage('Unit Tests') {
            steps {
                  echo('Run Unit tests')
                  git url: env.git_url, branch: branch_to_build, credentialsId: 'cicd-demo-scmsecret'
                  sh 'mvn test'
            }
        }

        stage('Test Build') {
            steps {
                // In ideal world this pipeline would also configure the build
                // as it is very simple one.
                echo 'Triggering the preconfigured build in OpenShift'
                script {
                    git url: "${git_url}", branch: "${branch_to_build}", credentialsId: 'cicd-demo-scmsecret'

                    openshift.withCluster() {
                        openshift.withProject("${test_project}") {
                            echo "Using project: ${openshift.project()}"
                            openshiftBuild(buildConfig: "${application}", verbose: false, namespace: "${test_project}")
                        }
                    }
                }
            }
        }
        stage('test Deploy') {
            steps {
                echo 'Triggering deploy in OpenShift'
                script {
                    openshift.withCluster() {
                        openshift.withProject("${test_project}") {
                            echo "Using project: ${openshift.project()}"
                            // First import image from Artifactory to local ImageStream
                            // sh 'oc import-image ${application} --all --from=artifactory.cygate.fi/docker-local/${test_project}/${application} --confirm -n ${test_project}'
                            openshiftDeploy(depCfg: "${application}", namespace: "${test_project}", verbose: false)
                            openshiftVerifyDeployment(depCfg: "${application}", namespace: "${test_project}")
                        }
                    }
                }
            }
        }

        stage('Static analysis') {
            steps {
                withCredentials([
                        //string(credentialsId: 'sonar-login-credential', variable: 'SONARQUBE')
                ]) {
                    echo('Run Static analysis')
                    // sh "mvn verify sonar:sonar -Dsonar.projectName=${sonar_project} -Dsonar.host.url=${sonar_host} -Dsonar.login=$SONARQUBE"
                }
            }
        }
        stage('Security') {
            steps {
                echo 'Run Security tests'
                echo('NOT IMPLEMENTED')
            }
        }

        // Manual Yes|No decision required before promoting build to QA
        stage('Should we proceed to QA') {
            steps {
                input(
                        message: "Should we deploy the version to QA?",
                        ok: "Yes"
                        //parameters
                )
                echo 'Going to QA'
            }
        }

        // Tags image to Artifactory with RC-YYYY-MM-X name and deploys to qa_project
        stage('Deploy QA') {

            steps {
                echo "====  {env.application}        =    ${env.application}"
                echo "====  {env.BUILD_DISPLAY_NAME} =    ${env.BUILD_DISPLAY_NAME}"
                echo "====  {env.BUILD_TAG}          =    ${env.BUILD_TAG}"

                echo "Pushing tag ${env.BUILD_TAG} from project ${env.test_project} to project ${env.qa_project}"

                script {

                    src_repo = env.test_project + '/' + env.application
                    target_repo = env.qa_project + '/' + env.application
                    date = new Date().format('YYYY-MM')
                    target_tag = 'RC-' + date + '-' + env.BUILD_DISPLAY_NAME.replaceAll("#", "")

                    openshift.withCluster() {
                        openshift.withProject("${env.qa_project}") {
                            echo "Patching the DeploymentConfig to follow new version"
                            openshift.patch("dc/${application}", """'{"spec":{"triggers":[{"type": "ImageChange", "imageChangeParams":{"automatic": true, "containerNames": ["${
                                application
                            }"], "from":{"kind": "ImageStreamTag", "namespace": "${qa_project}", "name": "${
                                application
                            }:${target_tag}"}}}]}}'""")
                            sh "oc tag ${test_project}/${application}:${test_tag} ${qa_project}/${application}:${target_tag}"
                            echo "Deploying new version"
                            openshiftDeploy(depCfg: "${application}", namespace: "${qa_project}", verbose: false)
                            echo "Verifying"
                            openshiftVerifyDeployment(depCfg: "${application}", namespace: "${qa_project}")
                        }
                    }
                }
            }

        }

        // After deployment tag git-repository commit with imagename
        stage('Tag git with build identifier') {
            steps {
                echo "Tagging git with build-identifier"
                git url: "${git_url}", branch: "${branch_to_build}", credentialsId: 'jenkins-push-key'
                sh "git config --global user.email 'versionhallinta@foobar.fi'"
                sh "git config --global user.name 'Versionhallinta Jenkins'"
                // Use SSH-agent for git
                sshagent(['jenkins-push-key'])
                        {
                            sh "git config --global user.email 'versionhallinta@foobar.fi'"
                            sh "git config --global user.name 'Versionhallinta Jenkins'"
                            sh("git tag -a ${target_tag} -m 'Jenkins tagging'")
                            sh("git push --tags")
                        }
            }
        }
        // Manual Yes|No decision required before promoting build to PROD
        stage('Should we proceed to PROD') {
            steps {
                input(
                        message: "Should we deploy the version to PROD?",
                        ok: "Yes"
                        //parameters
                )
                echo 'Going to PROD'
            }
        }

        stage('Deploying to PROD') {
            steps {
                script {
                    date = new Date().format('YYYY-MM')
                    // PROD-image is without RC-prefix
                    target_tag_prod = date + '-' + env.BUILD_DISPLAY_NAME.replaceAll("#", "")
                    src_repo = env.qa_project + '/' + env.application
                    target_repo = env.prod_project + '/' + env.application
                    date = new Date().format('YYYY-MM')

                    openshift.withCluster() {
                        openshift.withProject("${env.prod_project}") {
                            echo "Patching the DeploymentConfig to follow new version"
                            openshift.patch("dc/${application}", """'{"spec":{"triggers":[{"type": "ImageChange", "imageChangeParams":{"automatic": true, "containerNames": ["${
                                application
                            }"], "from":{"kind": "ImageStreamTag", "namespace": "${prod_project}", "name": "${
                                application
                            }:${target_tag_prod}"}}}]}}'""")
                            echo "Fetching new tags"
                            sh "oc tag ${qa_project}/${application}:${target_tag} ${prod_project}/${application}:${target_tag_prod}"
                            echo "Deploying new version to production"
                            openshiftDeploy(depCfg: "${application}", namespace: "${prod_project}", verbose: false)
                            echo "Verifying"
                            openshiftVerifyDeployment(depCfg: "${application}", namespace: "${prod_project}")
                        }
                    }
                }
            }
        }

        stage('Tag git with PROD tag') {
            steps {
                echo "Tagging git with build-identifier"
                git url: "${git_url}", branch: "${branch_to_build}", credentialsId: 'jenkins-push-key'
                sh "git config --global user.email 'versionhallinta@foobar.fi'"
                sh "git config --global user.name 'Versionhallinta Jenkins'"
                // Use SSH-agent for git
                sshagent(['jenkins-push-key'])
                        {
                            sh "git config --global user.email 'versionhallinta@foobar.fi'"
                            sh "git config --global user.name 'Versionhallinta Jenkins'"
                            sh("git tag -a ${target_tag_prod} -m 'Jenkins tagging'")
                            sh("git push --tags")
                        }
            }
        }
    }
}
