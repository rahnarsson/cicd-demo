package hello;

import javax.annotation.Resource;
import javax.annotation.Resources;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.context.properties.ConfigurationProperties;

// Spring Boot 2.x
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
 
// Spring Boot 1.x
//import org.springframework.boot.web.support.SpringBootServletInitializer;
  
@SpringBootApplication
public class OpenAcademyDemoApp extends SpringBootServletInitializer {
  
    public static void main(String[] args) {
        SpringApplication.run(applicationClass, args);
    }
  
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }
  
    private static Class<OpenAcademyDemoApp> applicationClass = OpenAcademyDemoApp.class;
}


@RestController
@Profile("dev")
class DevHelloController {
    @RequestMapping("/hello/{name}")
    String hello(@PathVariable String name) {

        return "Hello OpenAcademy from SpringBoot dev environment with mod:  "  + name + " !";
  
    }
}

@RestController
@Profile("qa")
class QAHelloController {
    @RequestMapping("/hello/{name}")
    String hello(@PathVariable String name) {

        return "Hello from SpringBoot QA environment: "  + name + " !";
  
    }
}

@RestController
@Profile("prod")
class PRODHelloController {
    @RequestMapping("/hello/{name}")
    String hello(@PathVariable String name) {

        return "Hello from SpringBoot PROD environment: "  + name + " !";
  
    }
}
