package hello;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.beans.Transient;

import hello.HelloController;

public class DevHelloControllerTest {

    @Test
    public void testDevHelloController() {
        HelloController helloController = new HelloController();
        String result  = helloController.home();
        assertEquals(result, "Hello OpenAcademy!");
    }
}